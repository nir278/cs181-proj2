// BUGFIX LOG:
// -- free memory when returning and add { } to ifs
// -- "<=" instead of "<" on slotNum range checks
//    functions: readRecord, deleteRecord, updateRecord 
// -- slot directory is at top of page, records are at bottom
// -- forgot to update slots
// -- forgot to check record status in slots

#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <string>

#include "rbfm.h"

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = NULL;
PagedFileManager *RecordBasedFileManager::_pf_manager = NULL;

RBFM_ScanIterator::RBFM_ScanIterator() {
    _rbf_manager = RecordBasedFileManager::instance();
}

RBFM_ScanIterator::~RBFM_ScanIterator() {
    
}

// Read two pointers as integers and return their comparison result
bool RBFM_ScanIterator::comparison_int(void *op1) {
    int op1_int = *(int*)op1;
    int op2_int = *(int*)compValue;

    switch (compOp) {
        case EQ_OP:
            return op1_int == op2_int;
        break;
        case LT_OP:
            return op1_int < op2_int;
        break;
        case LE_OP:
            return op1_int <= op2_int;
        break;
        case GT_OP:
            return op1_int > op2_int;
        break;
        case GE_OP:
            return op1_int >= op2_int;
        break;
        case NE_OP:
            return op1_int != op2_int;
        break;
        case NO_OP:
        default:
            return true;
        break;
    }

    return false;
}

// Read two pointers as floats and return their comparison result
bool RBFM_ScanIterator::comparison_real(void *op1) {
    float op1_float = *(float*)op1;
    float op2_float = *(float*)compValue;

    switch (compOp) {
        case EQ_OP:
            return op1_float == op2_float;
        break;
        case LT_OP:
            return op1_float < op2_float;
        break;
        case LE_OP:
            return op1_float <= op2_float;
        break;
        case GT_OP:
            return op1_float > op2_float;
        break;
        case GE_OP:
            return op1_float >= op2_float;
        break;
        case NE_OP:
            return op1_float != op2_float;
        break;
        case NO_OP:
        default:
            return true;
        break;
    }

    return false;
}

// Read two pointers as C-strings and return their comparison result
bool RBFM_ScanIterator::comparison_varchar(void *op1) {
    char* op1_cstr = (char*)op1;
    char* op2_cstr = (char*)compValue;
    int c = strcmp(op1_cstr, op2_cstr);
    switch (compOp) {
        case EQ_OP:
            return c == 0;
        break;
        case LT_OP:
            return c < 0;
        break;
        case LE_OP:
            return c <= 0;
        break;
        case GT_OP:
            return c > 0;
        break;
        case GE_OP:
            return c >= 0;
        break;
        case NE_OP:
            return c != 0;
        break;
        case NO_OP:
        default:
            return true;
        break;
    }

    return false;
}

// Project the attributes indicated by RBFM_ScanIterator::attributeNames
// Format is [null indicators][data]
RC RBFM_ScanIterator::projectRecord(const RID &rid, void *data) {
    int rc;

    // Read the record indicated by "rid"
    char recordData[PAGE_SIZE];
    rc = _rbf_manager->readRecord(fileHandle, recordDescriptor, rid, recordData);
    if (rc != SUCCESS) return rc;

    // Prepare the null indicators in "data"
    int nullIndicatorSize = _rbf_manager->getNullIndicatorSize(attributeNames.size());
    memset(data, 0, nullIndicatorSize);

    // The offsets in data to which projected data will be written
    int data_offset = nullIndicatorSize;

    // For each attribute name in attributeNames
    // The attribute
    Attribute attribute;
    // The attribute data in the record that was read
    char attributeData[ATTR_VALUE_MAXLEN];
    // If a VarChar attribute, need to know the string size
    int str_size;
    for (unsigned i=0; i<attributeNames.size(); i++) {
        // Get the attribute, read its data from the record
        attribute = recordDescriptor[_rbf_manager->getAttributeIndex(recordDescriptor, attributeNames[i])];
        rc = _rbf_manager->readAttribute(fileHandle, recordDescriptor, rid, attribute.name, attributeData);
        
        // Failed to read the attribute
        if (!(rc == RBFM_ATTR_IS_NULL || rc == SUCCESS))
            return rc;

        if (rc == RBFM_ATTR_IS_NULL) {
            // Attribute data is NULL, reflect in the null indicators
            int indicatorIndex = i / CHAR_BIT;
            int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
            ((char*)data)[indicatorIndex] |= indicatorMask;
        } else if (rc == SUCCESS) {
            // Attribute data is not NULL
            // Copy into "data" at data_offset
            switch (attribute.type) {
                case TypeInt:
                    memcpy((char*)data+data_offset, attributeData, sizeof(int));
                    data_offset += sizeof(int);
                    break;
                case TypeReal:
                    memcpy((char*)data+data_offset, attributeData, sizeof(float));
                    data_offset += sizeof(float);
                    break;
                case TypeVarChar:
                    // VarChar data must be prefixed with string size
                    str_size = *(int*)attributeData;
                    memcpy((char*)data+data_offset, attributeData, VARCHAR_LENGTH_SIZE+str_size);
                    data_offset += VARCHAR_LENGTH_SIZE + str_size;
                default:
                    break;
            }
        }
    }

    return SUCCESS;
}

// Build list of all RIDs in the heap file
// Build list of forwarded-to RIDs in the heap file
RC RBFM_ScanIterator::build_rids_lists() {
    int rc;
    char pageData[PAGE_SIZE];
    unsigned cur_page = 0;
    unsigned cur_slot = 0;
    unsigned num_slots = 0;

    RID rid;

    while (1) {
        // If reading the first slot of the next page,
        //      Ensure the next page exists, has records
        //      Load the next page
        if (cur_slot == 0) {
            if (cur_page < num_pages) {
                rc = fileHandle.readPage(cur_page, pageData);
                if (rc != SUCCESS)
                    return rc;

                SlotDirectoryHeader sdh = _rbf_manager->getSlotDirectoryHeader(pageData);
                num_slots = sdh.recordEntriesNumber;
                if (num_slots == 0)
                    break;
            } else {
                // EOF
                break;
            }
        }

        // Current RID
        rid.pageNum = cur_page;
        rid.slotNum = cur_slot;

        // Push current RID onto rids_all
        rids_all.push_back(rid);

        // If RID forwards,
        // Push forwarded-to RID onto rids_forwardedto
        SlotDirectoryRecordEntry sdre = _rbf_manager->getSlotDirectoryRecordEntry(pageData, cur_slot);
        if (sdre.offset < 0) {
            rid.pageNum = sdre.length;
            rid.slotNum = -sdre.offset;
            rids_forwardedto.push_back(rid);
        }

        // Move to the next RID
        // Move on to the next page if all RIDs on this page have been visited
        if (++cur_slot == num_slots) {
            cur_page++;
            cur_slot = 0;
        }
    }

    return SUCCESS;
}

// Project RBFM_ScanIterator::attributeNames
// From the next record
// Where RBFM_ScanIterator::conditionAttribute ::compOp ::compValue == true
RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data) {
    int rc;

    if (compOp == NO_OP) {
        rc = getNextRID(rid);
        if (rc == RBFM_EOF) return rc;
        return projectRecord(rid, data);
    }

    Attribute attribute = recordDescriptor[_rbf_manager->getAttributeIndex(recordDescriptor, conditionAttribute)];
    char attributeData[ATTR_VALUE_MAXLEN];

    bool comparison_result;

    // Scan all records
    while (1) {
        rc = getNextRID(rid);
        if (rc == RBFM_EOF) return rc;

        // Read the attribute data
        memset(attributeData, 0, ATTR_VALUE_MAXLEN);
        rc = _rbf_manager->readAttribute(fileHandle, recordDescriptor, rid, conditionAttribute, attributeData);
        
        if (!(rc == RBFM_ATTR_IS_NULL || rc == SUCCESS)) {
            return rc;
        }
        if (compValue == NULL) {
            comparison_result = (rc == RBFM_ATTR_IS_NULL);
        } else if (rc == RBFM_ATTR_IS_NULL) {
            comparison_result = (compValue == NULL);
        } else if (rc == SUCCESS) {
            // Obtain the comparison result
            switch (attribute.type) {
                case TypeInt:
                    comparison_result = comparison_int(attributeData);
                    break;
                case TypeReal:
                    comparison_result = comparison_real(attributeData);
                    break;
                case TypeVarChar:
                    comparison_result = comparison_varchar(attributeData+VARCHAR_LENGTH_SIZE);
                    break;
                default:
                    comparison_result = false;
                    break;
            }
        }

        // Return the original RID of the record
        // Project attributeNames from the record
        if (comparison_result) {
            return projectRecord(rid, data);
        }
    }
}

// Return next top-level RID
RC RBFM_ScanIterator::getNextRID(RID &rid) {

    while (1) {
        if (rids_all.size() == 0)
            return RBFM_EOF;

        rid = rids_all.back();
        rids_all.pop_back();

        bool forwardedTo = false;
        for (vector<RID>::iterator it=rids_forwardedto.begin();
                                   it!=rids_forwardedto.end();
                                   it++) {
            if (it->pageNum == rid.pageNum &&
                it->slotNum == rid.slotNum) {
                    forwardedTo = true;
                    break;
            }
        }
        if (!forwardedTo)
            break;
    }

    return SUCCESS;
}

RC RBFM_ScanIterator::close() {
    // compValue is malloc'd
    if (compValue)
        free(compValue);

    return _rbf_manager->closeFile(fileHandle);
}

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager) {
        _rbf_manager = new RecordBasedFileManager();
    }

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
    // Initialize the internal PagedFileManager instance
    _pf_manager = PagedFileManager::instance();
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

RC RecordBasedFileManager::createFile(const string &fileName) 
{
    // Creating a new paged file.
    RC rc = SUCCESS;
    rc = _pf_manager->createFile(fileName);
    if (rc != SUCCESS) {
        return rc;
    }
    // Setting up the first page.
    void * firstPageData = calloc(PAGE_SIZE, 1);
    if (firstPageData == NULL) {
        return RBFM_MALLOC_FAILED;
    }
    newRecordBasedPage(firstPageData);

    // Adds the first record based page.
    FileHandle handle;
    if (_pf_manager->openFile(fileName.c_str(), handle)) {
        free (firstPageData);
        return RBFM_OPEN_FAILED;
    }
    if (handle.appendPage(firstPageData)) {
        free (firstPageData);
        return RBFM_APPEND_FAILED;
    }
    _pf_manager->closeFile(handle);

    free(firstPageData);

    return SUCCESS;
}

RC RecordBasedFileManager::destroyFile(const string &fileName) 
{
    return _pf_manager->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) 
{
    return _pf_manager->openFile(fileName.c_str(), fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) 
{
    return _pf_manager->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) 
{
    // Gets the size of the record.
    unsigned recordSize = getRecordSize(recordDescriptor, data);

    // Cycles through pages looking for enough free space for the new entry.
    void *pageData = malloc(PAGE_SIZE);
    if (pageData == NULL) {
        return RBFM_MALLOC_FAILED;
    }
    bool pageFound = false;
    unsigned i;
    unsigned numPages = fileHandle.getNumberOfPages();
    for (i = 0; i < numPages; i++)
    {
        if (fileHandle.readPage(i, pageData)) {
            free (pageData);
            return RBFM_READ_FAILED;
        }

        // When we find a page with enough space (accounting also for the size that will be added to the slot directory), we stop the loop.
        if (getPageFreeSpaceSize(pageData) >= sizeof(SlotDirectoryRecordEntry) + recordSize)
        {
            pageFound = true;
            break;
        }
    }

    // If we can't find a page with enough space, we create a new one
    if (!pageFound)
    {
        newRecordBasedPage(pageData);
    }

    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);

    // Setting the return RID.
    rid.pageNum = i;
    rid.slotNum = slotHeader.recordEntriesNumber;

    // Adding the new record reference in the slot directory.
    SlotDirectoryRecordEntry newRecordEntry;
    newRecordEntry.length = recordSize;
    newRecordEntry.offset = slotHeader.freeSpaceOffset - recordSize;
    setSlotDirectoryRecordEntry(pageData, rid.slotNum, newRecordEntry);

    // Updating the slot directory header.
    slotHeader.freeSpaceOffset = newRecordEntry.offset;
    slotHeader.recordEntriesNumber += 1;
    setSlotDirectoryHeader(pageData, slotHeader);

    // Adding the record data.
    setRecordAtOffset (pageData, newRecordEntry.offset, recordDescriptor, data);

    // Writing the page to disk.
    if (pageFound)
    {
        if (fileHandle.writePage(i, pageData)) {
            free(pageData); 
            return RBFM_WRITE_FAILED;
        }
    }
    else
    {
        if (fileHandle.appendPage(pageData)) {
            free(pageData); 
            return RBFM_APPEND_FAILED;
        }
    }

    free(pageData);
    return SUCCESS;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) 
{
    // Retrieve the specific page
    void * pageData = malloc(PAGE_SIZE);
    if (fileHandle.readPage(rid.pageNum, pageData)) {
        free(pageData); 
        return RBFM_READ_FAILED;
    }

    // Checks if the specific slot id exists in the page
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);
    if (slotHeader.recordEntriesNumber <= rid.slotNum) {
        free(pageData); 
        return RBFM_SLOT_DN_EXIST;
    }

    // Gets the slot directory record entry data
    SlotDirectoryRecordEntry recordEntry = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

    // Checks if data exists
    if (recordEntry.length == 0) {
        free(pageData); 
        return RBFM_RECORD_DN_EXIST;
    }
    // Checks if data was moved 
    if (recordEntry.offset < 0) {
        RID movedRid;
        movedRid.pageNum = recordEntry.length;
        movedRid.slotNum = -recordEntry.offset;

        readRecord(fileHandle, recordDescriptor, movedRid, data);
    } else {
        // Retrieve the actual entry data
        getRecordAtOffset(pageData, recordEntry.offset, recordDescriptor, data);
    }

    free(pageData);
    return SUCCESS;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) 
{
    // Parse the null indicator into an array
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, data, nullIndicatorSize);
    
    // We've read in the null indicator, so we can skip past it now
    unsigned offset = nullIndicatorSize;

    cout << "----" << endl;
    for (unsigned i = 0; i < (unsigned) recordDescriptor.size(); i++)
    {
        cout << setw(10) << left << recordDescriptor[i].name << ": ";
        // If the field is null, don't print it
        bool isNull = fieldIsNull(nullIndicator, i);
        if (isNull)
        {
            cout << "NULL" << endl;
            continue;
        }
        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                uint32_t data_integer;
                memcpy(&data_integer, ((char*) data + offset), INT_SIZE);
                offset += INT_SIZE;

                cout << "" << data_integer << endl;
            break;
            case TypeReal:
                float data_real;
                memcpy(&data_real, ((char*) data + offset), REAL_SIZE);
                offset += REAL_SIZE;

                cout << "" << data_real << endl;
            break;
            case TypeVarChar:
                // First VARCHAR_LENGTH_SIZE bytes describe the varchar length
                uint32_t varcharSize;
                memcpy(&varcharSize, ((char*) data + offset), VARCHAR_LENGTH_SIZE);
                offset += VARCHAR_LENGTH_SIZE;

                // Gets the actual string.
                char *data_string = (char*) malloc(varcharSize + 1);
                if (data_string == NULL) {
                    return RBFM_MALLOC_FAILED;
                }
                memcpy(data_string, ((char*) data + offset), varcharSize);

                // Adds the string terminator.
                data_string[varcharSize] = '\0';
                offset += varcharSize;

                cout << data_string << endl;
                free(data_string);
            break;
        }
    }
    cout << "----" << endl;

    return SUCCESS;
}

// Private helper methods

// Configures a new record based page, and puts it in "page".
void RecordBasedFileManager::newRecordBasedPage(void * page)
{
    memset(page, 0, PAGE_SIZE);
    // Writes the slot directory header.
    SlotDirectoryHeader slotHeader;
    slotHeader.freeSpaceOffset = PAGE_SIZE;
    slotHeader.recordEntriesNumber = 0;
    setSlotDirectoryHeader(page, slotHeader);
}

SlotDirectoryHeader RecordBasedFileManager::getSlotDirectoryHeader(void * page)
{
    // Getting the slot directory header.
    SlotDirectoryHeader slotHeader;
    memcpy (&slotHeader, page, sizeof(SlotDirectoryHeader));
    return slotHeader;
}

void RecordBasedFileManager::setSlotDirectoryHeader(void * page, SlotDirectoryHeader slotHeader)
{
    // Setting the slot directory header.
    memcpy (page, &slotHeader, sizeof(SlotDirectoryHeader));
}

SlotDirectoryRecordEntry RecordBasedFileManager::getSlotDirectoryRecordEntry(void * page, unsigned recordEntryNumber)
{
    // Getting the slot directory entry data.
    SlotDirectoryRecordEntry recordEntry;
    memcpy  (
            &recordEntry,
            ((char*) page + sizeof(SlotDirectoryHeader) + recordEntryNumber * sizeof(SlotDirectoryRecordEntry)),
            sizeof(SlotDirectoryRecordEntry)
            );

    return recordEntry;
}

void RecordBasedFileManager::setSlotDirectoryRecordEntry(void * page, unsigned recordEntryNumber, SlotDirectoryRecordEntry recordEntry)
{
    // Setting the slot directory entry data.
    memcpy  (
            ((char*) page + sizeof(SlotDirectoryHeader) + recordEntryNumber * sizeof(SlotDirectoryRecordEntry)),
            &recordEntry,
            sizeof(SlotDirectoryRecordEntry)
            );
}

// Computes the free space of a page (function of the free space pointer and the slot directory size).
unsigned RecordBasedFileManager::getPageFreeSpaceSize(void * page) 
{
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(page);
    return slotHeader.freeSpaceOffset - slotHeader.recordEntriesNumber * sizeof(SlotDirectoryRecordEntry) - sizeof(SlotDirectoryHeader);
}

unsigned RecordBasedFileManager::getRecordSize(const vector<Attribute> &recordDescriptor, const void *data) 
{
    // Read in the null indicator
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, (char*) data, nullIndicatorSize);

    // Offset into *data. Start just after null indicator
    unsigned offset = nullIndicatorSize;
    // Running count of size. Initialize to size of header
    unsigned size = sizeof (RecordLength) + (recordDescriptor.size()) * sizeof(ColumnOffset) + nullIndicatorSize;

    for (unsigned i = 0; i < (unsigned) recordDescriptor.size(); i++)
    {
        // Skip null fields
        if (fieldIsNull(nullIndicator, i)) {
            continue;
        }
        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                size += INT_SIZE;
                offset += INT_SIZE;
            break;
            case TypeReal:
                size += REAL_SIZE;
                offset += REAL_SIZE;
            break;
            case TypeVarChar:
                uint32_t varcharSize = 0;
                // We have to get the size of the VarChar field by reading the integer that precedes the string value itself
                memcpy(&varcharSize, data + offset, VARCHAR_LENGTH_SIZE);
                size += varcharSize;
                offset += varcharSize + VARCHAR_LENGTH_SIZE;
            break;
        }
    }

    return size;
}

// Calculate actual bytes for nulls-indicator for the given field counts
int RecordBasedFileManager::getNullIndicatorSize(int fieldCount) 
{
    return int(ceil((double) fieldCount / CHAR_BIT));
}

bool RecordBasedFileManager::fieldIsNull(char *nullIndicator, int i)
{
    int indicatorIndex = i / CHAR_BIT;
    int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
    return (nullIndicator[indicatorIndex] & indicatorMask) != 0;
}

void RecordBasedFileManager::setRecordAtOffset(void *page, unsigned offset, const vector<Attribute> &recordDescriptor, const void *data)
{
    // Read in the null indicator
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset (nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, (char*) data, nullIndicatorSize);

    // Points to start of record
    char *start = (char*) page + offset;

    // Offset into *data
    unsigned data_offset = nullIndicatorSize;
    // Offset into page header
    unsigned header_offset = 0;

    RecordLength len = recordDescriptor.size();
    memcpy(start + header_offset, &len, sizeof(len));
    header_offset += sizeof(len);

    memcpy(start + header_offset, nullIndicator, nullIndicatorSize);
    header_offset += nullIndicatorSize;

    // Keeps track of the offset of each record
    // Offset is relative to the start of the record and points to the END of a field
    ColumnOffset rec_offset = header_offset + (recordDescriptor.size()) * sizeof(ColumnOffset);

    unsigned i = 0;
    for (i = 0; i < recordDescriptor.size(); i++)
    {
        if (!fieldIsNull(nullIndicator, i))
        {
            // Points to current position in *data
            char *data_start = (char*) data + data_offset;

            // Read in the data for the next column, point rec_offset to end of newly inserted data
            switch (recordDescriptor[i].type)
            {
                case TypeInt:
                    memcpy (start + rec_offset, data_start, INT_SIZE);
                    rec_offset += INT_SIZE;
                    data_offset += INT_SIZE;
                break;
                case TypeReal:
                    memcpy (start + rec_offset, data_start, REAL_SIZE);
                    rec_offset += REAL_SIZE;
                    data_offset += REAL_SIZE;
                break;
                case TypeVarChar:
                    unsigned varcharSize;
                    // We have to get the size of the VarChar field by reading the integer that precedes the string value itself
                    memcpy(&varcharSize, data_start, VARCHAR_LENGTH_SIZE);
                    memcpy(start + rec_offset, data_start + VARCHAR_LENGTH_SIZE, varcharSize);
                    // We also have to account for the overhead given by that integer.
                    rec_offset += varcharSize;
                    data_offset += VARCHAR_LENGTH_SIZE + varcharSize;
                break;
            }
        }
        // Copy offset into record header
        // Offset is relative to the start of the record and points to END of field
        memcpy(start + header_offset, &rec_offset, sizeof(ColumnOffset));
        header_offset += sizeof(ColumnOffset);
    }
}

// Support header size and null indicator. If size is less than recordDescriptor size, then trailing records are null
// Memset null indicator as 1?
void RecordBasedFileManager::getRecordAtOffset(void *page, unsigned offset, const vector<Attribute> &recordDescriptor, void *data)
{
    // Pointer to start of record
    char *start = (char*) page + offset;

    // Allocate space for null indicator. The returned null indicator may be larger than
    // the null indicator in the table has had fields added to it
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);

    // Get number of columns and size of the null indicator for this record
    RecordLength len = 0;
    memcpy (&len, start, sizeof(RecordLength));
    int recordNullIndicatorSize = getNullIndicatorSize(len);

    // Read in the existing null indicator
    memcpy (nullIndicator, start + sizeof(RecordLength), recordNullIndicatorSize);

    // If this new recordDescriptor has had fields added to it, we set all of the new fields to null
    for (unsigned i = len; i < recordDescriptor.size(); i++)
    {
        int indicatorIndex = (i+1) / CHAR_BIT;
        int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
        nullIndicator[indicatorIndex] |= indicatorMask;
    }
    // Write out null indicator
    memcpy(data, nullIndicator, nullIndicatorSize);

    // Initialize some offsets
    // rec_offset: points to data in the record. We move this forward as we read data from our record
    unsigned rec_offset = sizeof(RecordLength) + recordNullIndicatorSize + len * sizeof(ColumnOffset);
    // data_offset: points to our current place in the output data. We move this forward as we write data to data.
    unsigned data_offset = nullIndicatorSize;
    // directory_base: points to the start of our directory of indices
    char *directory_base = start + sizeof(RecordLength) + recordNullIndicatorSize;
    
    for (unsigned i = 0; i < recordDescriptor.size(); i++)
    {
        if (fieldIsNull(nullIndicator, i)) {
            continue;
        }
        
        // Grab pointer to end of this column
        ColumnOffset endPointer;
        memcpy(&endPointer, directory_base + i * sizeof(ColumnOffset), sizeof(ColumnOffset));

        // rec_offset keeps track of start of column, so end-start = total size
        uint32_t fieldSize = endPointer - rec_offset;

        // Special case for varchar, we must give data the size of varchar first
        if (recordDescriptor[i].type == TypeVarChar)
        {
            memcpy((char*) data + data_offset, &fieldSize, VARCHAR_LENGTH_SIZE);
            data_offset += VARCHAR_LENGTH_SIZE;
        }
        // Next we copy bytes equal to the size of the field and increase our offsets
        memcpy((char*) data + data_offset, start + rec_offset, fieldSize);
        rec_offset += fieldSize;
        data_offset += fieldSize;
    }
}

RC RecordBasedFileManager::compactPage(void* page, unsigned gapStartOffset, unsigned gapSize) {

    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(page);
    
    // Check if the gap is properly in the record space 
    if (gapStartOffset < slotHeader.freeSpaceOffset) {
        return RBFM_COMPACT_FAILED;
    }
    unsigned dataSize = gapStartOffset - slotHeader.freeSpaceOffset;
    unsigned destOffset = slotHeader.freeSpaceOffset + gapSize;

    // Check if the move will be within the page
    if (destOffset + dataSize > PAGE_SIZE) {
        return RBFM_COMPACT_FAILED;
    }
  
    // Close gap  
    memmove((char*) page + destOffset, (char*) page + slotHeader.freeSpaceOffset, dataSize);

    // Run through the slots and change the offsets if they moved 
    for (int i = 0; i < slotHeader.recordEntriesNumber; i++) {
        SlotDirectoryRecordEntry currentRecordEntry = getSlotDirectoryRecordEntry(page, i);
        // Here we see which slots have moved  
        if (currentRecordEntry.offset > 0 && (unsigned)currentRecordEntry.offset <= gapStartOffset) {
            currentRecordEntry.offset += gapSize;
            setSlotDirectoryRecordEntry(page, i, currentRecordEntry);
        }
    }
 
    // Update slot directory
    slotHeader.freeSpaceOffset = destOffset;
    setSlotDirectoryHeader(page, slotHeader); 

    return SUCCESS;
} 
     

RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid) {

    // Retrieve the specific page
    void * pageData = malloc(PAGE_SIZE);
    if (pageData == NULL) {
        return RBFM_MALLOC_FAILED;
    }
    if (fileHandle.readPage(rid.pageNum, pageData)) {
        free(pageData);
        return RBFM_READ_FAILED;
    }

    // Check if the specific slot id exists in the page
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);
    if(slotHeader.recordEntriesNumber <= rid.slotNum) {
        free(pageData); 
        return RBFM_SLOT_DN_EXIST;
    }

    // Get the slot directory record entry data
    SlotDirectoryRecordEntry recordEntry = getSlotDirectoryRecordEntry(pageData, rid.slotNum);
     
    // Check if record exists
    if (recordEntry.length == 0) {
        free(pageData);
        return RBFM_RECORD_DN_EXIST;
    }

    // Check if the data has been moved
    if (recordEntry.offset < 0) {
        RID Nrid;
        Nrid.pageNum = recordEntry.length;
        Nrid.slotNum = (-1 * recordEntry.offset);

        // Delete moved record with recursive call 
        deleteRecord(fileHandle, recordDescriptor, Nrid);
    } else {
        // Else record exists, delete record and remove gap
        compactPage(pageData, recordEntry.offset, recordEntry.length);
    }

    // Zero out this slot
    recordEntry.length = 0;
    recordEntry.offset = -1;
    setSlotDirectoryRecordEntry(pageData, rid.slotNum, recordEntry);

    // Write over the current page
    fileHandle.writePage(rid.pageNum, pageData); 

    free(pageData);
    return SUCCESS;
}

// Assume the RID does not change after an update
RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid) {

    // Retrieve the specific page
    void * pageData = malloc(PAGE_SIZE);
    if (pageData == NULL) {
        return RBFM_MALLOC_FAILED;
    }
    if (fileHandle.readPage(rid.pageNum, pageData)) {
        free(pageData);
        return RBFM_READ_FAILED;
    }

    // Check if the slotID is valid 
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);
    if(slotHeader.recordEntriesNumber <= rid.slotNum) {
        free(pageData);
        return RBFM_SLOT_DN_EXIST;
    }

    // Get the slot directory record entry data
    SlotDirectoryRecordEntry recordEntry = getSlotDirectoryRecordEntry(pageData, rid.slotNum);
     
    // Get the size of the update data  
    unsigned updateSize = getRecordSize(recordDescriptor, data);

    // Check if record exists
    if (recordEntry.length == 0) {
        free(pageData);
        return RBFM_RECORD_DN_EXIST;
    }

    // Check if the data has been moved
    if (recordEntry.offset < 0) {
        RID Nrid;
        Nrid.pageNum = recordEntry.length;
        Nrid.slotNum = (-1 * recordEntry.offset);

        free(pageData);

        // Update moved record with recursive call
        return updateRecord(fileHandle, recordDescriptor, data, Nrid);
    } 
    
    // Update page record if record fits
    if (updateSize <= recordEntry.length) {
        setRecordAtOffset(pageData, recordEntry.offset, recordDescriptor, data);
        // Compact page if update was smaller than orginal record
        if (updateSize < recordEntry.length) {
           unsigned originalSize = recordEntry.length; 
           
           // Update length first, so that compact takes care of offset
           recordEntry.length = updateSize;
           setSlotDirectoryRecordEntry(pageData, rid.slotNum, recordEntry);
           
           // Compact page
           compactPage(pageData, recordEntry.offset + updateSize, originalSize - updateSize);
        }
       
    } else {
        // Record grew so we have to remove current record
        // and insert the updated Record into the freespace
    
        // Remove current record
        compactPage(pageData, recordEntry.offset, recordEntry.length);
    
        // Check if we have enough freespace
        unsigned freeSpace = getPageFreeSpaceSize(pageData);
        if (freeSpace >= updateSize) {
            
            // Get new freespaceOffset 
            slotHeader = getSlotDirectoryHeader(pageData); 
          
            // Prepare record entry to the new offset and updateSize
            recordEntry.length = updateSize;
            recordEntry.offset = slotHeader.freeSpaceOffset - updateSize; 
    
            // Insert record at new offset
            setRecordAtOffset(pageData, recordEntry.offset, recordDescriptor, data);
    
            // Update slot directory header
            slotHeader.freeSpaceOffset -= updateSize;
            setSlotDirectoryHeader(pageData, slotHeader);
             
        } else {
            // Prepare an RID to get the record's new location
            RID moveRid = {0,0};
    
            // Insert record into new page and get the move location
            int insert_rc = insertRecord(fileHandle, recordDescriptor, data, moveRid);
            if (insert_rc != SUCCESS) {
                free(pageData);
                return insert_rc;
            }
          
            // Update record entry with moved data info. 
            // A negative offset for the slotNum is used to indicate the redirection 
            recordEntry.length = moveRid.pageNum;
            recordEntry.offset = -moveRid.slotNum;
    
        }

        // Set updated record entry
        setSlotDirectoryRecordEntry(pageData, rid.slotNum, recordEntry);
    }

    // Flush changes to disk
    fileHandle.writePage(rid.pageNum, pageData); 

    free(pageData);
    return SUCCESS;

}

int RecordBasedFileManager::getAttributeIndex(const vector<Attribute> &recordDescriptor, const string &attributeName) {
    for (unsigned i = 0; i < recordDescriptor.size(); ++i) {
        if (recordDescriptor[i].name == attributeName) {
            return i;
        }
    }
    return -1;
}


RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data) {
    // Retrieve the specific page
    void * pageData = malloc(PAGE_SIZE);
    if (pageData == NULL) {
        return RBFM_MALLOC_FAILED;
    }
    if (fileHandle.readPage(rid.pageNum, pageData)) {
        free(pageData);
        return RBFM_READ_FAILED;
    }

    // Checks if the specific slot id exists in the page
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);
    if (slotHeader.recordEntriesNumber <= rid.slotNum) {
        free(pageData); 
        return RBFM_SLOT_DN_EXIST;
    }

    // Gets the slot directory record entry data
    SlotDirectoryRecordEntry recordEntry = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

    // Checks if data exists
    if (recordEntry.length == 0) {
        free(pageData); 
        return RBFM_RECORD_DN_EXIST;
    }
    // Checks if data was moved 
    if (recordEntry.offset < 0) {
        RID movedRid;
        movedRid.pageNum = recordEntry.length;
        movedRid.slotNum = -recordEntry.offset;

        free(pageData);
        return readAttribute(fileHandle, recordDescriptor, movedRid, attributeName, data);
    }
     
    // Get attribute index
    int attributeIndex = getAttributeIndex(recordDescriptor, attributeName);
    
    if (attributeIndex == -1) {
        free(pageData); 
        return RBFM_ATTR_DN_EXIST;
    }

    // Pointer to start of record
    char *start = (char*) pageData + recordEntry.offset;

    // Get number of columns and size of the null indicator for this record
    RecordLength len = 0;
    memcpy (&len, start, sizeof(RecordLength));

    // Check if the attribute is null by default
    if (attributeIndex >= len) {
        free(pageData); 
        return RBFM_ATTR_IS_NULL;
    }

    // Check if attribute is NULL 
    unsigned nullIndicatorSize = getNullIndicatorSize(len);
    char nullIndicator[nullIndicatorSize];
    memcpy (nullIndicator, start + sizeof(RecordLength), nullIndicatorSize);
    if (fieldIsNull(nullIndicator, attributeIndex)) {
        free(pageData); 
        return RBFM_ATTR_IS_NULL;
    } else {
        // Attribute is stored
 
        // Get field_end offset
        unsigned header_offset = sizeof(RecordLength) + nullIndicatorSize;
        unsigned directory_offset = header_offset + sizeof(ColumnOffset) * attributeIndex;
        ColumnOffset field_end = 0;
        memcpy(&field_end, start + directory_offset, sizeof(ColumnOffset));

        // Get field_start offset
        ColumnOffset field_start = 0;
        if (attributeIndex == 0) {
            field_start =  header_offset + sizeof(ColumnOffset) * len;
        } else {
            memcpy(&field_start, start + directory_offset - sizeof(ColumnOffset), sizeof(ColumnOffset));
        }
        
        uint32_t fieldSize = field_end - field_start;
        
        // Set return attribute
        if (recordDescriptor[attributeIndex].type == TypeVarChar) {
            // If the Attribute is type VarChar then include the length
            memcpy((char*) data, &fieldSize, VARCHAR_LENGTH_SIZE);
            memcpy((char*) data + sizeof(uint32_t), start + field_start, fieldSize); 
        } else {
            memcpy(data, start + field_start, fieldSize); 
        }
    }         

    free(pageData); 
    return SUCCESS;
}

RC RecordBasedFileManager::scan(FileHandle &fileHandle,
    const vector<Attribute> &recordDescriptor,
    const string &conditionAttribute,
    const CompOp compOp,                  // comparison type such as "<" and "="
    const void *value,                    // used in the comparison
    const vector<string> &attributeNames, // a list of projected attributes
    RBFM_ScanIterator &rbfm_ScanIterator) {

    int rc;

    cerr << "rbfm::scan(): fileHandle.getFilename() = " << fileHandle.getFilename() << endl;
    rc = openFile(fileHandle.getFilename(), rbfm_ScanIterator.fileHandle);
    if (rc != SUCCESS)
        return rc;

    rbfm_ScanIterator.num_pages = rbfm_ScanIterator.fileHandle.getNumberOfPages();

    rbfm_ScanIterator.recordDescriptor.clear();
    for (unsigned i=0; i<recordDescriptor.size(); i++)
        rbfm_ScanIterator.recordDescriptor.push_back(recordDescriptor[i]);

    rbfm_ScanIterator.conditionAttribute = conditionAttribute;
    rbfm_ScanIterator.compOp = compOp;

    if (value == NULL) {
        rbfm_ScanIterator.compValue = NULL;
    } else {
        // value is const void*. Must make a copy.
        // This means value must be free'd in close().
        Attribute attribute = recordDescriptor[getAttributeIndex(recordDescriptor, conditionAttribute)];
        switch (attribute.type) {
            case TypeInt:
                rbfm_ScanIterator.compValue = malloc(sizeof(int));
                *(int*)rbfm_ScanIterator.compValue = *(int*)value;
                break;
            case TypeReal:
                rbfm_ScanIterator.compValue = malloc(sizeof(float));
                *(float*)rbfm_ScanIterator.compValue = *(float*)value;
                break;
            case TypeVarChar:
                rbfm_ScanIterator.compValue = strdup((char*)value);
                break;
            default:
                rbfm_ScanIterator.compValue = NULL;
                break;
        }
    }

    rbfm_ScanIterator.attributeNames.clear();
    for (unsigned i=0; i<attributeNames.size(); i++)
        rbfm_ScanIterator.attributeNames.push_back(attributeNames[i]);

    return rbfm_ScanIterator.build_rids_lists();
}
