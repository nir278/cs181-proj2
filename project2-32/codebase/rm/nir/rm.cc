// CAUTION: Nothing done with Column Rids at the moment!

#include <cassert>
#include <cmath>
#include <cstring>

#include "rm.h"


RelationManager* RelationManager::_rm = NULL;
RecordBasedFileManager* RelationManager::_rbfm = NULL;

// Calculate actual bytes for nulls-indicator for the given field counts
int getNullIndicatorSize(int fieldCount) 
{
    return int(ceil((double) fieldCount / CHAR_BIT));
}


RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager() :
        _rm_tableNames{"", "Tables", "Columns"},
        _rm_fileHandles(3),
        _rm_recordDescriptors(3),
        _file_suffix(".tbl"),
        _table_id(0)
{
    // Initialize the internal RecordBasedFileManager instance
    _rbfm = RecordBasedFileManager::instance();

    // Initialize catalog recordDescriptors
    initRecordDescriptor_Tables();
    initRecordDescriptor_Columns();
}

RelationManager::~RelationManager()
{
}

RC RelationManager::openTable(const string &tableName, FileHandle &fh) {
    return _rbfm->openFile(tableName + _file_suffix, fh);
}

RC RelationManager::closeTable(FileHandle &fh) {
    return _rbfm->closeFile(fh);
}

// Add an attribute to a record descriptor
void RelationManager::addToRecordDescriptor(vector<Attribute> &recordDescriptor, const string &name, AttrType type, AttrLength length) {
    Attribute attr; 
    attr.name = name;
    attr.type = type;
    attr.length = length;
    recordDescriptor.push_back(attr);
}

void RelationManager::initRecordDescriptor_Tables() {
    string attrName("table-id");
    addToRecordDescriptor(_rm_recordDescriptors[1], attrName, TypeInt, INT_SIZE);
    attrName = "table-name";
    addToRecordDescriptor(_rm_recordDescriptors[1], attrName, TypeVarChar, 50);
    attrName = "file-name";
    addToRecordDescriptor(_rm_recordDescriptors[1], attrName, TypeVarChar, 50);
    attrName = "init--count";
    addToRecordDescriptor(_rm_recordDescriptors[1], attrName, TypeInt, INT_SIZE);
}

void RelationManager::initRecordDescriptor_Columns() {
    string attrName("table-id");
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeInt, INT_SIZE);
    attrName = "column-name";
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeVarChar, 50);
    attrName = "column-type";
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeInt, INT_SIZE);
    attrName = "column-length";
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeInt, INT_SIZE);
    attrName = "column-position";
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeInt, INT_SIZE);
    attrName = "column-status";
    addToRecordDescriptor(_rm_recordDescriptors[2], attrName, TypeInt, INT_SIZE);
}

// Given a record descriptor, and pointers to corresponding data values,
// create the record data
// A null value is represented by a null pointer in valuePointers
int RelationManager::prepareRecord(const vector<Attribute> &recordDescriptor, vector<void*> valuePointers, void *data) {

    // prepare nullIndicator and data 
    unsigned nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    //void* nullIndicator = malloc(nullIndicatorSize);
    //memset(nullIndicator, 0, nullIndicatorSize);
    memset(data, 0, nullIndicatorSize);

    // setup data offset
    unsigned dataOffset = nullIndicatorSize;

    int32_t datum_int = 0;
    float   datum_real = 0.0;
    char   *datum_varchar = "";
    int32_t datum_varchar_size = 0;

    for (unsigned i=0; i<recordDescriptor.size(); i++) {
        fprintf(stderr, "prepareRecord():loop_%", i);
        // Check if field is NULL
        if (valuePointers[i] == NULL) {
            // Update nullIndicator
            int indicatorIndex = i / CHAR_BIT;
            int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
            char nullByte = 0;
            memcpy(&nullByte, data + indicatorIndex, 1); 
            nullByte |= indicatorMask;
            memcpy(data + indicatorIndex, &nullByte, 1);
        } else {
            switch (recordDescriptor[i].type)
            {
                case TypeInt:
                    datum_int = *(int32_t*)valuePointers[i];
                    memcpy((char*)data+dataOffset, &datum_int, INT_SIZE);
                    dataOffset += sizeof(int32_t);
                break;
                case TypeReal:
                    datum_real = *(float*)valuePointers[i];
                    memcpy((char*)data+dataOffset, &datum_real, sizeof(float));
                    dataOffset += sizeof(float);
                break;
                case TypeVarChar:
                    datum_varchar = (char*)valuePointers[i];
                    datum_varchar_size = strlen(datum_varchar);
                    memcpy((char*)data+dataOffset, &datum_varchar_size, VARCHAR_LENGTH_SIZE);
                    dataOffset += sizeof(int32_t);
                    memcpy((char*)data+dataOffset, datum_varchar, datum_varchar_size);
                    dataOffset += datum_varchar_size;
                default:
                break;
            }
        }
    }

    return dataOffset;
}

RC RelationManager::prepareRecord_Tables(TableNum tableId, const string &tableName, const string &tableFileName, int32_t num_columns, void *data) {

    if (tableName.size() + _file_suffix.size() > 50) {
        return RM_INVALID_TABLE;
    }

    vector<void*> recordValues;
    recordValues.push_back((void*)&tableId);
    recordValues.push_back((void*)tableName.c_str());
    recordValues.push_back((void*)tableFileName.c_str());

    prepareRecord(_rm_recordDescriptors[1], recordValues, data);

    return 0;
}

RC RelationManager::insertTableInfoIntoTables(const string &tableName, const vector<Attribute> &attrs, TableNum &tableId) {
    ++_table_id;

    RC rc = SUCCESS;
    tableId = _table_id;

    void* record = malloc(PAGE_SIZE - 1);
    if (record == NULL) {
       return RM_MALLOC_FAILED;
    }

    rc = prepareRecord_Tables(tableId, tableName, tableName + _file_suffix, attrs.size(), record);

    if (rc != 0) {
        free(record);
        return rc;
    }

    RID rid;
    rc = _rbfm->insertRecord(_rm_fileHandles[1], attrs, record, rid);
    
    _table_rids.push_back(rid);
    free(record);
    return rc;
} 

// Pre: varchars have proper length
void RelationManager::prepareRecord_Columns(TableNum tableId, const string &columnName, AttrType columnType, AttrLength columnLength, ColumnNum columnPosition, ColumnStatus columnStatus, void* data) {
    
    vector<void*> recordValues;
    recordValues.push_back((void*)&tableId);
    recordValues.push_back((void*)columnName.c_str());
    recordValues.push_back((void*)&columnType);
    recordValues.push_back((void*)&columnLength);
    recordValues.push_back((void*)&columnPosition);
    recordValues.push_back((void*)&columnStatus);

    prepareRecord(_rm_recordDescriptors[2], recordValues, data);

}

// CAUTION: Nothing done with Column Rids at the moment!!
// Pre: valid recordDescriptor with varchars having proper length
RC RelationManager::insertTableInfoIntoColumns(const string &tableName, const vector<Attribute> &attrs, const TableNum tableId) {
    RC rc = SUCCESS;

    void* record = malloc(PAGE_SIZE - 1);
    if (record == NULL) {
       return RM_MALLOC_FAILED;
    }

    // Insert each Attribute's info into the Columns table
    RID dummyRid;
    for (ColumnNum iColumn = 0; iColumn < attrs.size(); ++iColumn) {
        prepareRecord_Columns(tableId, attrs[iColumn].name, attrs[iColumn].type, 
                              attrs[iColumn].length, iColumn + 1, ColumnStatus_Stored, record);
        rc = _rbfm->insertRecord(_rm_fileHandles[2], attrs, record, dummyRid);
        if (rc != SUCCESS) {
            free(record);
            return rc;
        }
    }
    
    free(record);
    return rc;
} 

RC RelationManager::insertTableInfoIntoCatalog(const string &tableName, const vector<Attribute> &attrs, const TableNum tableId) {
    TableNum tableId;
    rc = insertTableInfoIntoTables(tableName, attrs, tableId);
    if (rc != SUCCESS) {
       return rc;
    } 
    rc = insertTableInfoIntoColumns(tableName, attrs, tableId);
    return rc; 
}

RC RelationManager::createCatalog()
{
    assert(_rm_tableNames.size() >= 2);
    assert(_rm_tableNames.size() == _rm_fileHandles.size());
    RC rc = SUCCESS;

    // Create and open catalog files
    for (unsigned iTable = 0; iTable < _rm_tableNames.size(); ++iTable) {
        rc = _rbfm->createFile(_rm_tableNames[iTable] + _file_suffix);
        if (rc != SUCCESS) {
            return rc;
        }
        rc = openTable(_rm_tableNames[iTable], _rm_fileHandles[iTable]);
        if (rc != SUCCESS) {
            return rc;
        }
    }

    // Insert catalog info into catalog
    TableNum tableId = 0;
    for (int iTable = 0; iTable < _rm_tableNames.size(); ++iTable) {
        rc = insertTableInfoIntoCatalog(_rm_tableNames[iTable], _rm_recordDescriptors[iTable], tableId);
        if (rc != SUCCESS) {
            return rc;
        }
    }
    return rc;
}

RC RelationManager::deleteCatalog()
{
    //deletes the catalog by deleting each table, then finaly the catalog
    
    return -1;
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    RC rc = SUCCESS;
    
    // Create table file
    rc = _rbfm->createFile(tableName + _file_suffix);
    if (rc != SUCCESS) {
       return rc;
    } 
    
    // Insert table info into catalog
    rc = insertTableInfoIntoCatalog(_rm_tableNames[iTable], _rm_recordDescriptors[iTable], tableId);
    return rc;
}

RC RelationManager::deleteTable(const string &tableName)
{
    // Check if table is a catalog table
    for (unsigned iTable = 1; iTable < _rm_tableNames.size(); ++iTable) {
        if (tableName == _rm_tableNames[iTable]) {
            return RM_INVALID_TABLE; 
        }
    }
        
    //with the name given, pull up table 1, get the ID number for tableName and the RID where the data is stored, then update table 1 to not have this 
    //data, go to the location givin in step one and scan trhough the location for RID's using deleate record to clear each location, then clear the 
    //table, finaly go to table 2, pull it put, clear all entrys deaing with the table ID, and update table 2
    return -1;
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    //go to table 1, get the ID for table name, the go to table 2, collect the attributes for tableName, write them to attrs
    return -1;
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    //go to table 1 and get the location for tablename for the data location, make an insert the data into a record on a page, take that RID and add
    //it to the record for the table data, then update that record 
    return -1;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    //go to table 1 to get the location of the data for tableName, find the RID that maches the one we were given, remove it and then update record,
   //then deleate where the rid points with deleate record
    return -1;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    //check to make sure the table exsist, then just use update record on the RID we given 
    return -1;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    //go to the RID we are given and use readrecord
    return -1;
}

RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
     //print the data we are given, used for debug so
	return -1;
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    //go to table 1, get the location of the data and the table ID, next go to table 2 to find where the attributes for ID are stored, read them till 
    //we find attributename and save the column position, then go to the RID we have and return the data at the position we got:
    return -1;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    return -1;
}



