#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cassert>
#include <cmath>
#include <cstring>

#include "rm.h"

const int catalog_varchar_length = 50;
const int max_catalog_record_size = PAGE_SIZE;

RelationManager* RelationManager::_rm = NULL;
RecordBasedFileManager* RelationManager::_rbfm = NULL;

// Debug print functions
void RelationManager::printRecordDesc(const vector<Attribute> &rd) {
    cerr << ">> printRecordDesc()" << endl;
    cerr << endl;
    for (unsigned i = 0; i < rd.size(); ++i) {
        cerr << rd[i].name << ", " << rd[i].type << ", " << rd[i].length << endl;
    }
    cerr << endl;
    cerr << "<< printRecordDesc()" << endl;
}

void RelationManager::printRecordInfo(const RecordInfo &ri) {
    cerr << endl << "nulls: ";
    for (unsigned i = 0; i < ri.nulls.size(); ++i) {
        cerr << ri.nulls[i];
    }
    cerr << endl;

    cerr << endl << "isInts: ";
    for (unsigned i = 0; i < ri.ints.size(); ++i) {
        cerr << ri.ints[i] << " / ";
    }
    cerr << endl << "isReals: ";
    for (unsigned i = 0; i < ri.reals.size(); ++i) {
        cerr << ri.reals[i] << " / ";
    }
    cerr << endl << "varChars: ";
    for (unsigned i = 0; i < ri.varChars.size(); ++i) {
        cerr << ri.varChars[i] << " / ";
    }
    cerr << endl;
}

void RelationManager::printStrings(const vector<string> &strings) {
   for (unsigned i = 0; i < strings.size(); ++i) {
       cerr << strings[i] << endl;
   }
}



RC RelationManager::printRids(FileHandle &fileHandle, const vector<Attribute> &recordDesc, const vector<RID> &rids, unsigned count, unsigned start) {
    cerr << ">> printRids()" << endl;
    cerr << "rids.size() = " << rids.size() << endl;    
    assert(count + start <= rids.size());
    
    RC rc = SUCCESS;
    void* returnedData = malloc(PAGE_SIZE);
    if (returnedData == NULL) {
        fprintf(stderr, "printRids(): MALLOC FAILED");
        return RM_MALLOC_FAILED;
    }
    for (unsigned i = start; i < count + start; i++) {
       // Given the rid, read the record from file
       rc = _rbfm->readRecord(fileHandle, recordDesc, rids[i], returnedData);
       if (rc != SUCCESS) {
           cerr << "FAIL: printRids() readRecord for rids[" << i << "] failed" << endl; 
           cerr << "early done" << endl;
           free(returnedData);
           return rc;
       }
       cerr << endl << "Returned Data:" << endl;
       cerr << "RID { " << rids[i].pageNum << ", " << rids[i].slotNum << " }" << endl;
       _rbfm->printRecord(recordDesc, returnedData);
    }
    free(returnedData);
    cerr << "<< printRids()" << endl;
    return rc;
}

void printNullInd(char* nullIndicator, unsigned nullIndicatorSize) {
    cerr << "nullIndicatorSize = " << nullIndicatorSize << endl;
    for (unsigned i = 0; i < nullIndicatorSize; i++) {
        cerr << "nullByte[" << i << "] = " << (int)nullIndicator[i] << endl;
    }
}

// Constructors and Destructors ------------------------

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager() 
{
    // Initialize the internal RecordBasedFileManager instance
    _rbfm = RecordBasedFileManager::instance();

    _table_id = 0;

    _rm_catalogNames.push_back("Tables");
    _rm_catalogNames.push_back("Columns");

    FileHandle fh;
    RecordInfo ri;
    
    fprintf(stderr, "_rm_catalogNames.size() = %d\n", _rm_catalogNames.size());
    
    for (unsigned i = 0; i < _rm_catalogNames.size(); ++i) {
        _rm_catalogHandles.push_back(fh);
        _rm_catalogInfos.push_back(ri);
        _rm_catalogQueries.push_back(ri);
    }

    // Initialize catalog recordDescriptors
    initCatalogDescriptor_Tables();
    initCatalogDescriptor_Columns();
    initCatalogResults_Tables();
}

RelationManager::~RelationManager()
{
}

// Table Handles -----------------------------

RC RelationManager::openTable(const string &tableName, FileHandle &fh) {
    return _rbfm->openFile(tableName, fh);
}

RC RelationManager::closeTable(FileHandle &fh) {
    return _rbfm->closeFile(fh);
}

// Misc Useful -----------------------------

// Calculate actual bytes for nulls-indicator for the given field counts
int getNullIndicatorSize(int fieldCount) 
{
    return int(ceil((double) fieldCount / CHAR_BIT));
}

bool fieldIsNull(char *nullIndicator, int i)
{
    int indicatorIndex = i / CHAR_BIT;
    int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
    return (nullIndicator[indicatorIndex] & indicatorMask) != 0;
}


// Add an attribute to a record descriptor
void RelationManager::addToRecordDescriptor(vector<Attribute> &recordDesc, const string &name, AttrType type, AttrLength length) {
    Attribute attr; 
    attr.name = name;
    attr.type = type;
    attr.length = length;
    recordDesc.push_back(attr);
}

// Given a record descriptor, and pointers to corresponding data values,
// create the record data
// A null value is represented by a null pointer in valuePointers
int RelationManager::prepareRecord(const vector<Attribute> &recordDesc, vector<void*> valuePointers, void *data) {

    // prepare nullIndicator and data 
    unsigned nullIndicatorSize = getNullIndicatorSize(recordDesc.size());
    //void* nullIndicator = malloc(nullIndicatorSize);
    //memset(nullIndicator, 0, nullIndicatorSize);
    memset(data, 0, nullIndicatorSize);

    // setup data offset
    unsigned dataOffset = nullIndicatorSize;

    int32_t datum_int = 0;
    float   datum_real = 0.0;
    char   *datum_varchar = "";
    int32_t datum_varchar_size = 0;

    for (unsigned i=0; i<recordDesc.size(); i++) {
        fprintf(stderr, "prepareRecord():loop_%", i);
        // Check if field is NULL
        if (valuePointers[i] == NULL) {
            // Update nullIndicator
            int indicatorIndex = i / CHAR_BIT;
            int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
            char nullByte = 0;
            memcpy(&nullByte, data + indicatorIndex, 1); 
            nullByte |= indicatorMask;
            memcpy(data + indicatorIndex, &nullByte, 1);
        } else {
            switch (recordDesc[i].type)
            {
                case TypeInt:
                    datum_int = *(int32_t*)valuePointers[i];
                    memcpy((char*)data+dataOffset, &datum_int, INT_SIZE);
                    dataOffset += sizeof(int32_t);
                break;
                case TypeReal:
                    datum_real = *(float*)valuePointers[i];
                    memcpy((char*)data+dataOffset, &datum_real, sizeof(float));
                    dataOffset += sizeof(float);
                break;
                case TypeVarChar:
                    datum_varchar = (char*)valuePointers[i];
                    datum_varchar_size = strlen(datum_varchar);
                    memcpy((char*)data+dataOffset, &datum_varchar_size, VARCHAR_LENGTH_SIZE);
                    dataOffset += sizeof(int32_t);
                    memcpy((char*)data+dataOffset, datum_varchar, datum_varchar_size);
                    dataOffset += datum_varchar_size;
                default:
                break;
            }
        }
    }

    return dataOffset;
}

RC RelationManager::getDataFromRecordInfo(const RecordInfo &recordInfo, void* data) {
    // prepare nullIndicator and data 
    unsigned nullIndicatorSize = getNullIndicatorSize(recordInfo.recordDesc.size());
    //void* nullIndicator = malloc(nullIndicatorSize);
    //memset(nullIndicator, 0, nullIndicatorSize);
    memset(data, 0, nullIndicatorSize);

    // setup data offset
    unsigned dataOffset = nullIndicatorSize;

    unsigned iInt = 0;
    unsigned iReal = 0;
    unsigned iVarChar = 0;

    for (unsigned i=0; i<recordInfo.recordDesc.size(); i++) {
        // Check if field is NULL
        if (recordInfo.nulls[i] == true) {
            // Update nullIndicator
            int indicatorIndex = i / CHAR_BIT;
            int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
            char nullByte = 0;
            memcpy(&nullByte, data + indicatorIndex, 1); 
            nullByte |= indicatorMask;
            memcpy(data + indicatorIndex, &nullByte, 1);
        } else {
            switch (recordInfo.recordDesc[i].type)
            {
                case TypeInt:
                    memcpy((char*)data+dataOffset, &recordInfo.ints.at(iInt), INT_SIZE);
                    dataOffset += sizeof(int32_t);
                break;
                case TypeReal:
                    memcpy((char*)data+dataOffset, &recordInfo.reals.at(iReal), sizeof(float));
                    dataOffset += sizeof(float);
                break;
                case TypeVarChar:
                {
                    uint32_t varCharSize = recordInfo.varChars.at(iVarChar).size();
                    memcpy((char*)data+dataOffset, &varCharSize, VARCHAR_LENGTH_SIZE);
                    dataOffset += sizeof(int32_t);
                    memcpy((char*)data+dataOffset, &recordInfo.varChars.at(iVarChar), varCharSize);
                    dataOffset += varCharSize;
                }
                default:
                break;
            }
        }
    }

    return dataOffset;
}

// Pre: recordInfo.recordDesc is set 
RC RelationManager::getRecordInfoFromData(const void* data, RecordInfo &recordInfo) {
    cerr << ">> **getRecordInfoFromData()" << endl;
//DELETE THIS--
    cerr << "#@*&%)* data!! (*&(*&" << endl;
    _rbfm->printRecord(recordInfo.recordDesc, data);
    cerr << "#@*&%)* data!! (*&(*&" << endl;
//--DELETE ABOVE    

    recordInfo.clearValues();
    // Parse the null indicator into an array
    int nullIndicatorSize = getNullIndicatorSize(recordInfo.recordDesc.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, data, nullIndicatorSize);
    
    // We've read in the null indicator, so we can skip past it now
    unsigned offset = nullIndicatorSize;

    for (unsigned i = 0; i < recordInfo.recordDesc.size(); i++)
    {
        string cpp_string;
        bool isNull = fieldIsNull(nullIndicator, i);
        recordInfo.hasSomeNull += isNull;
        recordInfo.nulls.push_back(isNull);
        // Add value if the field is not null
        if (!isNull) {
            switch (recordInfo.recordDesc[i].type)
            {
                case TypeInt:
                    uint32_t data_integer;
                    memcpy(&data_integer, ((char*) data + offset), INT_SIZE);
                    offset += INT_SIZE;
                    
                    recordInfo.ints.push_back(data_integer);
                break;
                case TypeReal:
                    float data_real;
                    memcpy(&data_real, ((char*) data + offset), REAL_SIZE);
                    offset += REAL_SIZE;

                    recordInfo.reals.push_back(data_real);
                break;
                case TypeVarChar:
                    // First VARCHAR_LENGTH_SIZE bytes describe the varchar length
                    uint32_t varcharSize;
                    memcpy(&varcharSize, ((char*) data + offset), VARCHAR_LENGTH_SIZE);
                    offset += VARCHAR_LENGTH_SIZE;

                    // Gets the actual string.
                    char *data_string = (char*) malloc(varcharSize + 1);
                    if (data_string == NULL) {
                        return RBFM_MALLOC_FAILED;
                    }
                    memcpy(data_string, ((char*) data + offset), varcharSize);

                    // Adds the string terminator.
                    data_string[varcharSize] = '\0';
                    offset += varcharSize;

                    cpp_string = data_string; 
                    recordInfo.varChars.push_back(cpp_string);
                    free(data_string);
                break;
            }
        }
    }
    //DELETE THIS--
    cerr << "#@*&%)* data!! (*&(*&" << endl;
    printRecordInfo(recordInfo);
    cerr << "#@*&%)* done printRecordInfo!! (*&(*&" << endl;
//--DELETE ABOVE    



    cerr << ">> **getRecordInfoFromData()" << endl;
    return SUCCESS;
}

RC RelationManager::getSubRecordDescriptor(const vector<Attribute> &origRecordDesc, vector<Attribute> &newRecordDesc, const vector<string> attrNames) {
    newRecordDesc.clear();
    for (unsigned iAttrName; iAttrName < attrNames.size(); ++iAttrName) {
        bool found = false;
        for (unsigned iOrigAttr = 0; iOrigAttr < origRecordDesc.size(); ++iOrigAttr) {
            if (origRecordDesc[iOrigAttr].name == attrNames[iAttrName]) {
                newRecordDesc.push_back(origRecordDesc[iOrigAttr]);
                found = true;
                break;
            }
        }
        if (!found) {
           return RM_COLUMN_DN_EXIST; 
        }
    }
    return SUCCESS;
}


// RM Helpers =============================== 

// Table Checks -----------------------------

// A check for write functions
bool RelationManager::isSystemTable(const string &tableName) {
    for (unsigned iTable = 1; iTable < _rm_catalogNames.size(); ++iTable) {
        if (tableName == _rm_catalogNames[iTable]) {
            return true;  
        }
      }
     return false;
}

// Catalog helpers

RC RelationManager::insertTableIntoCatalog(const string &tableName, const vector<Attribute> &attrs) {
    cerr << ">> insertTablesIntoCatalog()" << endl;
    RC rc = SUCCESS;
    TableNum tableId;
    rc = insertTableIntoTables(tableName, attrs, tableId);
    if (rc != SUCCESS) {
       return rc;
    } 
    rc = insertColumnsIntoColumns(tableId, attrs);
    cerr << "<< insertTablesIntoCatalog()" << endl;
    return rc; 
}

RC RelationManager::getTableInfo(const string &tableName, TableNum &tableId) {
    RID dummy;
    return getTableInfo(tableName, dummy, tableId);
}

RC RelationManager::getTableInfo(const string &tableName, RID &rid, TableNum &tableId) {
    ColumnNum dummy1 = 0;
    ColumnNum dummy2 = 0;
    return getTableInfo(tableName, rid, tableId, dummy1, dummy2);
}

// A check for read and write functions 
// note: openTable checks if file exists
RC RelationManager::getTableInfo(const string &tableName, RID &rid, TableNum &table_id, ColumnNum &orig_size, ColumnNum &last_column) {
     cerr << ">> $$$$$$$$getTableInfo()$$$$$$$$$$$$$$$$$$$$" << endl; 
     RC rc = SUCCESS;

     cerr << endl << "========DESCRIPS AND ATTRNAMES=========" << endl;
     cerr << "MAIN_RecordDesc" << ":" << endl;
     printRecordDesc(_rm_catalogInfos[0].recordDesc);
     
     cerr << endl << "PROJECTED_RecordDesc" << ":" << endl;
     printRecordDesc(_rm_catalogQueries[0].recordDesc);

     cerr << endl << "PROJECTED_ATTRNAMES" << ":" << endl;
     printStrings(_rm_catalogQueries[0].attrNames);
  
     cerr << endl << "========================================" << endl;

     RBFM_ScanIterator rbfm_si;

     // Search Tables for table  
     rc = _rbfm->scan(
             _rm_catalogHandles[0], // &fileHandle
             _rm_catalogInfos[0].recordDesc, 
                                    // const auto& recordDesc 
             "table-name",          // const string& conditionAttrib 
             EQ_OP,                 // CompOp compOp 
             tableName.c_str(),     // const void* value
             _rm_catalogQueries[0].attrNames, 
                                    // const vector<string>& attrNames
             rbfm_si);              // &rbfm_ScanItor 

     // Check if table is in Tables (assert: no duplicates)
     if (rc != SUCCESS) {
         return rc;
     } 

     cerr << "Table Scan RC == SUCCESS" << endl;

     void* returnedData = malloc(PAGE_SIZE);
     if (returnedData == NULL) {
         return RM_MALLOC_FAILED;
     }     

     if (rbfm_si.getNextRecord(rid, returnedData) == RBFM_EOF) {
         free(returnedData);
         return RM_TABLE_DN_EXIST; 
     }

     cerr << "############ PRINT SCAN RESULT ##########" << endl;
     rc = _rbfm->printRecord(_rm_catalogQueries[0].recordDesc, returnedData);
     if (rc != SUCCESS) {
         free(returnedData);
         return rc;
     }
     cerr << "############ DONE SCAN RESULT ##########" << endl;

     // Get results (the function clears old values)
     getRecordInfoFromData(returnedData, _rm_catalogQueries[0]);     
     
     // WARNING: HARDCODED! Change this if catalogQueries[0] changes!
     if (_rm_catalogQueries[0].ints.size() != 3) {
         cerr << "QUERIES INTS.SIZE() != 3" << endl;
         free(returnedData);
         return RM_INVALID_RESULT;
     }
     table_id = _rm_catalogQueries[0].ints[0];
     orig_size = _rm_catalogQueries[0].ints[1];
     last_column = _rm_catalogQueries[0].ints[2];


     cerr << "############ READ RID RESULT ##########" << endl;
     rc = _rbfm->readRecord(_rm_catalogHandles[0], _rm_catalogInfos[0].recordDesc, rid, returnedData);
     if (rc != SUCCESS) {
         free(returnedData);
         return rc;
     }
     rc = _rbfm->printRecord(_rm_catalogInfos[0].recordDesc, returnedData);
     if (rc != SUCCESS) {
         free(returnedData);
         return rc;
     }
     cerr << "############ DONE RID RESULT ##########" << endl;
     rc = getRecordInfoFromData(returnedData, _rm_catalogInfos[0]);
     if (rc != SUCCESS) {
         cerr << "getRecordInfoFromData failed" << endl;
         free(returnedData);
         return rc;
     }    

     rc = rbfm_si.close();
     free(returnedData);
     cerr << "<< $$$$$$$$$$getTableInfo()$$$$$$$$$$$$$" << endl; 
     return rc;
}

// Possible Ways of Returning Attributes:
// 1. orig                // col_position <= orig_size
// 2. orig-origdels       // col_position <= orig_size & !column_null
// 3. orig-origdels+added // !column_null
// 4. orig+origdels+added   
// l. origdels
// terminology:
// storedData aka recordData --> returnedData --> returnData aka resultData aka tupleData
// @return sorted columns aka ALL_ATTRS
//     column(table-id:int, column-name:varchar, column-type: int,  
//            column-length:int, column-position:int, column-null:int);
//     columns{ column_position{column-name, column-type, column-length}, column_pos{c.. } 
// @return deleteIndexes: list of ORIGDELS indexes (they will still be stored) 
RC RelationManager::getTableColumnsInfo(TableNum table_id, ColumnNum orig_size, ColumnNum last_column, vector<Attribute> &tableColumns, vector<unsigned> &origDeleteIndexes, vector<RID> rids) {
    RBFM_ScanIterator rbfm_si;

     cerr << endl << "========DESCRIPS AND ATTRNAMES=========" << endl;
     cerr << "MAIN AND PROJECTED RecordDesc" << ":" << endl;
     printRecordDesc(_rm_catalogInfos[1].recordDesc);

     cerr << endl << "PROJECTED AttrNames" << ":" << endl;
     printStrings(_rm_catalogInfos[1].attrNames);
  
     cerr << endl << "========================================" << endl;

    // setup scan
    RC rc = _rbfm->scan(
             _rm_catalogHandles[1],          // &fileHandle
             _rm_catalogInfos[1].recordDesc, // const auto& recordDesc 
             "table-id",                     // const string& conditionAttrib 
             EQ_OP,                          // CompOp compOp 
             &table_id,                      // const void* value
             _rm_catalogInfos[1].attrNames,  // const vector<string>& attrNames
             rbfm_si);                       // &rbfm_ScanItor 
    if (rc != SUCCESS) {
         return rc;
    } 
    
    // allocate returnedData
    void* returnedData = malloc(PAGE_SIZE);
    if (returnedData == NULL) {
        return RM_MALLOC_FAILED;
    }     
    memset(returnedData, 0, PAGE_SIZE); 
/*
    // MOVE THIS:
    // find number of initial result columns
    unsigned size = (attrsType == ORIG_ATTRS || attrsType == ORIG_MINUS_DELS) ?
                    orig_size : last_column;

    returnedDescriptor.resize(size);
    // reminder: size represents the last return column position prior to decrement
*/
    // setup vector size
    tableColumns.resize(last_column);
    cerr << "last_column = " << last_column << endl;
    cerr << "TableColumns.size() = " << tableColumns.size() << endl;
    origDeleteIndexes.clear();

    RID rid;
    Attribute attr; 
    unsigned assignedCount = 0;

    // iterate
    while (assignedCount < last_column &&  
         rbfm_si.getNextRecord(rid, returnedData) != RBFM_EOF) {
         rids.push_back(rid);
         // WARNING: HARDCODED. MUST CHANGE IF RD changes
         // These values must be stored correctly
         // recordInfo(
         //   nulls{ //fieldNulls}
         //   ints{0.table-id, 1.column-type, 2.column-length, 3.column-position, 4.column-null)}
         //   reals{}
         //   string{column-name}
        
         // Get data values
         // note: fields of returnedData are in order
         cerr << "############ PRINT SCAN RESULT ##########" << endl;
         _rbfm->printRecord(_rm_catalogQueries[0].recordDesc, returnedData);
         cerr << "############ DONE SCAN RESULT ##########" << endl;

         getRecordInfoFromData(returnedData, _rm_catalogInfos[1]);
 
         assert(_rm_catalogInfos[1].hasSomeNull == false && "All ColumnInfo fields are mandatory");
         assert(_rm_catalogInfos[1].ints.size() == 5);

         cerr << "############ READ RID RESULT ##########" << endl;
         _rbfm->readRecord(_rm_catalogHandles[0], _rm_catalogInfos[0].recordDesc, rid, returnedData);
         _rbfm->printRecord(_rm_catalogInfos[0].recordDesc, returnedData);
         cerr << "############ DONE RID RESULT ##########" << endl;
         getRecordInfoFromData(returnedData, _rm_catalogInfos[0]);
 
         ColumnNum column_position = _rm_catalogInfos[1].ints[3];
         --column_position;   // note the decrement, careful of OBOE

         // reminder: the "column" itself can be null, but the info about the column cannot
         NullType column_null     = _rm_catalogInfos[1].ints[4];
         
         // reminder: column_position was decremented above
         // reminder: first column_position should have been 1 and now 0, careful of OBOE! 
         //           WARNING: potential overflow if not matching!
         if (column_position < last_column) {
             ++assignedCount; 
             tableColumns[column_position].name   = _rm_catalogInfos[1].varChars[0];
             tableColumns[column_position].type   = static_cast<AttrType>(_rm_catalogInfos[1].ints[1]);
             tableColumns[column_position].length = _rm_catalogInfos[1].ints[2];
             if ( column_position < orig_size && column_null != TypeNotNull) {
                 origDeleteIndexes.push_back(column_position);
             }
          }
    }

    rbfm_si.close();
    free(returnedData);
    return rc; 
}

// Member functions ============================

RC RelationManager::createCatalog()
{
    cerr << ">> createCatalog()" << endl;
    assert(_rm_catalogNames.size() >= 2);
    assert(_rm_catalogNames.size() == _rm_catalogHandles.size());
    RC rc = SUCCESS;

    // Create and open catalog files
    for (unsigned iTable = 0; iTable < _rm_catalogNames.size(); ++iTable) {
        rc = _rbfm->createFile(_rm_catalogNames[iTable]);
        if (rc != SUCCESS) {
            return rc;
        }
        rc = openTable(_rm_catalogNames[iTable], _rm_catalogHandles[iTable]);
        if (rc != SUCCESS) {
            return rc;
        }
    }

    // Insert catalog into catalog
    for (unsigned iTable = 0; iTable < _rm_catalogNames.size(); ++iTable) {
        rc = insertTableIntoCatalog(_rm_catalogNames[iTable], _rm_catalogInfos[iTable].recordDesc);
        if (rc != SUCCESS) {
            return rc;
        }
    }
    cerr << "<< createCatalog()" << endl;
    return rc;
}

RC RelationManager::deleteCatalog()
{
    cerr << ">> deleteCatalog()" << endl;
    RC rc = SUCCESS;

    //deletes the catalog by deleting each table, then finaly the catalog
    for (unsigned iTable = 0; iTable < _rm_catalogNames.size(); ++iTable) {
        rc = _rbfm->closeFile(_rm_catalogHandles[iTable]);
        if (rc != SUCCESS) {
            return rc;
        }
        cerr << "deleteCatalog() fileName = " << _rm_catalogNames.size() << endl;
        rc = _rbfm->destroyFile(_rm_catalogNames[iTable]);
        if (rc != SUCCESS) {
            return rc;
        }
    }
    
    cerr << "<< deleteCatalog()" << endl;
    return rc;
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    cerr << ">> createTable()" << endl;
    // Check if valid table
    // NOTE: Do not have to check if is system table because create will fail
    if (tableName.empty()) {
        return RM_INVALID_TABLE;
    }

    RC rc = SUCCESS;
    // Create table file
    rc = _rbfm->createFile(tableName);
    if (rc != SUCCESS) {
       return rc;
    } 
    // Insert table info into catalog
    rc = insertTableIntoCatalog(tableName, attrs);
    cerr << "<< createTable()" << endl;
    return rc;
}

RC RelationManager::deleteTable(const string &tableName)
{
    cerr << ">> deleteTable()" << endl;
    // Check if table is a catalog table
    if (isSystemTable(tableName)) {
        return RM_INVALID_TABLE;
    }  

    RC rc = _rbfm->destroyFile(tableName);
    if (rc != SUCCESS) {
        return rc;
    }

    // get TableInfo
    RID rid;
    TableNum table_id;
    ColumnNum orig_size = 0;
    ColumnNum last_column = 0;
    rc = getTableInfo(tableName, rid, table_id, orig_size, last_column); 
    if (rc != SUCCESS) {
        return rc;
    }

    // delete Table from Tables
    rc = _rbfm->deleteRecord(_rm_catalogHandles[0], _rm_catalogInfos[0].recordDesc, rid);
    if (rc != SUCCESS) {
        return rc;
    }
    
    // get TableColumnsInfo
    vector<RID> rids;
    vector<Attribute> tableColumns;
    vector<ColumnNum>  dummyIndexes;
    rc = getTableColumnsInfo(table_id, orig_size, last_column, tableColumns, dummyIndexes, rids); 
    if (rc != SUCCESS) {
        return rc;
    }
 
    // delete Table from Tables
    for (unsigned i = 0; i < rids.size(); ++i) {
        RC rc = _rbfm->deleteRecord(_rm_catalogHandles[1], _rm_catalogInfos[1].recordDesc, rids[i]);
    } 

    cerr << "<< deleteTable()" << endl;
    return rc;
}


RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    //go to table 1, get the ID for table name, the go to table 2, collect the attributes for tableName, write them to attrs
    //once scan is implemented use here to get all the rids for the colloums rid's
//going to write a double for loop, read the column-positioin and I want to put them in the attrs in order
   //find way to get amount of coumloms 
    if (isSystemTable(tableName.c_str()) == false) return -1;
    printf("it all went so wrong\n");
    // get TableInfo
    RID rid;
    TableNum table_id;
    ColumnNum orig_size = 0;
    ColumnNum last_column = 0;
    RC rc = getTableInfo(tableName, rid, table_id, orig_size, last_column); 
    if (rc != SUCCESS) {
        return rc;
    }

    // get TableColumnsInfo
    vector<RID> rids;

    vector<ColumnNum>  dummyIndexes;
    rc = getTableColumnsInfo(table_id, orig_size, last_column, attrs, dummyIndexes, rids); 
    if (rc != SUCCESS) {
        return rc;
    }
}


RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    //go to table 1 and get the location for tablename for the data location, make an insert the data into a record on a page, take that RID and add
    //it to the record for the table data, then update that record
    //insert here check to make sure the table exsist
    RC rc;
    FileHandle temp;
    rc = isSystemTable(tableName);
    if(rc != SUCCESS) return rc;
    rc = openTable(tableName, temp);
    if(rc != SUCCESS) return rc;
    vector<Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if(rc != SUCCESS) return rc;    
    rc = _rbfm->insertRecord(temp, attrs, data,rid);
    if(rc != SUCCESS) return rc;
    closeTable(temp);
    return SUCCESS;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    //go to table 1 to get the location of the data for tableName, find the RID that maches the one we were given, remove it and then update record,
   //then deleate where the rid points with deleate record
    //insert here check to make sure the table exsist
    RC rc;
    FileHandle temp;
    rc = isSystemTable(tableName);
    if(rc != SUCCESS) return rc;
    rc = openTable(tableName, temp);
    if(rc != SUCCESS) return rc;
    vector<Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if(rc != SUCCESS) return rc;    
    rc = _rbfm->deleteRecord(temp , attrs , rid);
    if(rc != SUCCESS) return rc;
    closeTable(temp);
    return SUCCESS;

}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    //check to make sure the table exsist, then just use update record on the RID we given 
    //insert here check to make sure the table exsist
    RC rc;
    FileHandle temp;
    rc = isSystemTable(tableName);
    if(rc != SUCCESS) return rc;
    rc = openTable(tableName, temp);
    if(rc != SUCCESS) return rc;
    vector<Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if(rc != SUCCESS) return rc;    
    rc = _rbfm->updateRecord(temp, attrs, data, rid);
    if(rc != SUCCESS) return rc;
    closeTable(temp);
    return SUCCESS;

}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    //go to the RID we are given and use readrecord
    RC rc;
    FileHandle temp;
    rc = isSystemTable(tableName);
    if(rc != SUCCESS) return rc;
    rc = openTable(tableName, temp);
    if(rc != SUCCESS) return rc;
    vector<Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if(rc != SUCCESS) return rc;    
    rc = _rbfm->readRecord(temp, attrs, rid, data);
    if(rc != SUCCESS) return rc;
    closeTable(temp);
    return SUCCESS;


}

RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
    return _rbfm->printRecord(attrs, data);
}



RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
      // Get table id
      TableNum table_id = 0;
      RC rc = getTableInfo(tableName, table_id);
      if (rc != SUCCESS) {
          return rc;
      }
// get TableInfo
    RID trid;

    ColumnNum orig_size = 0;
    ColumnNum last_column = 0;
    rc = getTableInfo(tableName, trid, table_id, orig_size, last_column); 
    if (rc != SUCCESS) {
        return rc;
    }
    FileHandle fileHandle;
    rc = openTable(tableName, fileHandle);
    //now I should have my table and next I will get the data at the  given RID

      // Get RecordDescriptor from Columns
     //I first get the list of attributes then use that to call readAttribute
     //from the rbfm 
     vector<Attribute> recordDescriptor;
     getAttributes(tableName, recordDescriptor);
    _rbfm->readAttribute(fileHandle, recordDescriptor ,rid, attributeName, data);
    //go to table 1, get the location of the data and the table ID, next go to table 2 to find where the attributes for ID are stored, read them till 
    //we find attributename and save the column position, then go to the RID we have and return the data at the position we got:
    return -1;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
      FileHandle fileHandle;
      RC rc = openTable(tableName, fileHandle);
      if (rc != SUCCESS) {
          return rc;
      }

      RID rid;
      TableNum table_id = 0;
      rc = (tableName, rid, table_id);
      if (rc != SUCCESS) {
          return rc;
      }

      //TODO: get column info 

      // BONUS: need to several attrNames and recordDescs 
      RecordInfo ri; 
      /*  
      return _rbfm->scan(fileHandle,             // &fileHandle
                  ri.recordDesc,                 // TODO: const auto& recordDesc 
                  conditionAttribute,            // const string& conditionAttrib 
                  compOp,                        // CompOp compOp 
                  value,                         // const void* value
                  attributeNames,                // const vector<string>& attrNames
                  rm_ScanIterator);              // &rbfm_ScanItor 
      */
      return -1;
}











// PRIVATE SUBHELPERS ===========================

// Catalog RecordDescriptor Inits  --------------

void RelationManager::initCatalogDescriptor_Tables() {
    Attribute attr;

    attr.name = "table-id";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[0].recordDesc.push_back(attr);
 
    attr.name = "table-name";
    attr.type = TypeVarChar;
    attr.length = catalog_varchar_length;
    _rm_catalogInfos[0].recordDesc.push_back(attr);
     
    attr.name = "file-name";
    attr.type = TypeVarChar;
    attr.length = catalog_varchar_length;
    _rm_catalogInfos[0].recordDesc.push_back(attr);

    attr.name = "orig-size";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[0].recordDesc.push_back(attr);

    attr.name = "last-column";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[0].recordDesc.push_back(attr);

    _rm_catalogInfos[0].syncAttrNames();
    fprintf(stderr, "\ninitCatalogDescriptor_Tables(): _rm_rd[0]:\n");
    printRecordDesc(_rm_catalogInfos[0].recordDesc);
    printStrings(_rm_catalogInfos[0].attrNames);
}

void RelationManager::initCatalogDescriptor_Columns() {
    Attribute attr;

    attr.name = "table-id";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[1].recordDesc.push_back(attr);
 
    attr.name = "column-name";
    attr.type = TypeVarChar;
    attr.length = 50;
    _rm_catalogInfos[1].recordDesc.push_back(attr);
     
    attr.name = "column-type";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[1].recordDesc.push_back(attr);
     
    attr.name = "column-length";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[1].recordDesc.push_back(attr);

    attr.name = "column-position";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[1].recordDesc.push_back(attr);

    attr.name = "column-null";
    attr.type = TypeInt;
    attr.length = INT_SIZE;
    _rm_catalogInfos[1].recordDesc.push_back(attr);

    _rm_catalogInfos[1].syncAttrNames();
    fprintf(stderr, "\ninitCatalogDescriptor_Columns(): _rm_rd[1]:\n");
    printRecordDesc(_rm_catalogInfos[1].recordDesc);
    printStrings(_rm_catalogInfos[1].attrNames);
}

void RelationManager::initCatalogResults_Tables() {
    // initialize the projected attrNames for scan
    _rm_catalogQueries[0].attrNames.push_back("table-id");
    _rm_catalogQueries[0].attrNames.push_back("orig-size");
    _rm_catalogQueries[0].attrNames.push_back("last-column");
    
    // initialize the corresponding recordDesc 
    getSubRecordDescriptor(_rm_catalogInfos[0].recordDesc, _rm_catalogQueries[0].recordDesc, _rm_catalogQueries[0].attrNames);
    fprintf(stderr, "\ninitCatalogResults_tables(): _rm_catalogQueries.[0].recordDesc:\n");
    printRecordDesc(_rm_catalogQueries[0].recordDesc);
}

// Pre: tableName.size < _rm_catalog_varchar_length
RC RelationManager::insertTableIntoTables(const string &tableName, const vector<Attribute> &columns, TableNum &tableId) {
    cerr << ">> insertTableIntoTables()" << endl;

    RC rc = SUCCESS;
    ++_table_id; 
    tableId = _table_id;

    void *data = malloc(max_catalog_record_size);
    if (data == NULL) {
        return RM_MALLOC_FAILED;
    }
    // set nullIndicator
    unsigned offset = 0;
    int nullIndicatorSize = getNullIndicatorSize(_rm_catalogInfos[0].recordDesc.size()); 
    memset((char*)data, 0, nullIndicatorSize);
    offset += nullIndicatorSize;

    // set table-id 
    memcpy((char*)data + offset, &tableId, INT_SIZE);
    offset += INT_SIZE;

    // set table-name length
    uint32_t nameSize = tableName.size();
    memcpy((char*)data + offset, &nameSize, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;

    // set table-name
    memcpy((char*)data + offset, tableName.c_str(), nameSize);
    offset += nameSize;

     // set file-name length
    memcpy((char*)data + offset, &nameSize, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;

    // set file-name
    memcpy((char*)data + offset, tableName.c_str(), nameSize);
    offset += nameSize;

    // set orig-size
    int orig_size = columns.size();
    memcpy((char*)data + offset, &orig_size, INT_SIZE);
    offset += INT_SIZE;

    // set last-column 
    memcpy((char*)data + offset, &orig_size, INT_SIZE);

    // data preparation done
   
    // insert Table into Tables 
    RID dummyRid;
    rc = _rbfm->insertRecord(_rm_catalogHandles[0], _rm_catalogInfos[0].recordDesc, data, dummyRid);

    
    fprintf(stderr, "insertTableIntoTables():\n");
    vector<RID> debugRids;
    debugRids.push_back(dummyRid);

    //printRids(_rm_catalogHandles[0], _rm_catalogInfos.recordDesc, debugRids, 1, 0);
    
    free(data);
    cerr << "<< insertTableIntoTables()" << endl;
    return rc;
}

RC RelationManager::insertColumnIntoColumns(TableNum table_id, Attribute column, ColumnNum column_index, NullType column_null) {
    cerr << ">> insertColumnIntoColumns()" << endl;
    RC rc = SUCCESS;

    void *data = malloc(max_catalog_record_size);
    if (data == NULL) {
        return RM_MALLOC_FAILED;
    }

    // set nullIndicator
    unsigned offset = 0;
    int nullIndicatorSize = getNullIndicatorSize(_rm_catalogInfos[1].recordDesc.size()); 
    char nullIndicator[nullIndicatorSize] = {0};
    
    memcpy((char*)data, nullIndicator, nullIndicatorSize);
    offset += nullIndicatorSize;

    // set table-id
    memcpy((char*)data + offset, &table_id, INT_SIZE);
    offset += INT_SIZE;

    // set column-name.size() 
    AttrLength nameSize = static_cast<AttrLength>(column.name.size());
    memcpy((char*)data + offset, &nameSize, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;

    // set column-name
    memcpy((char*)data + offset, column.name.c_str(), nameSize);
    offset += nameSize;

    // set column-type
    uint32_t intNum = static_cast<uint32_t>(column.type);
    memcpy((char*)data + offset, &intNum, INT_SIZE);
    offset += INT_SIZE;

    // set column-length
    intNum = static_cast<uint32_t>(column.length);
    memcpy((char*)data + offset, &intNum, INT_SIZE);
    offset += INT_SIZE;

    // set column-position
    ++column_index;
    memcpy((char*)data + offset, &column_index, INT_SIZE);
    offset += INT_SIZE;

    // set column-null
    memcpy((char*)data + offset, &column_null, INT_SIZE);
    offset += INT_SIZE;
    // data preparation done

    // insert Column into Columns
    RID dummyRid;
    rc = _rbfm->insertRecord(_rm_catalogHandles[1], _rm_catalogInfos[1].recordDesc, data, dummyRid);
    
    fprintf(stderr, "insertColumnIntoColumns():\n");
    vector<RID> debugRids;
    debugRids.push_back(dummyRid);

    printRids(_rm_catalogHandles[1], _rm_catalogInfos[1].recordDesc, debugRids, 1, 0);

    free(data);
    cerr << "<< insertColumnIntoColumns()" << endl;
    return rc;
}

// Pre: valid recordDesc with varchars having proper length
RC RelationManager::insertColumnsIntoColumns(TableNum table_id, const vector<Attribute> &columns) {
    cerr << ">> insertColumnsIntoColumns()" << endl;
    RC rc = SUCCESS;
    for (ColumnNum iColumn = 0; iColumn < columns.size(); ++iColumn) {
        rc = insertColumnIntoColumns(table_id, columns[iColumn], iColumn, TypeNotNull);
        if (rc != SUCCESS) {
            return rc;
        }
    }
    cerr << "<< insertColumnsIntoColumns()" << endl;
    return rc;
} 

