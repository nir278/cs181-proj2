
#ifndef _rm_h_
#define _rm_h_

#include <string>
#include <vector>

#include "../rbf/rbfm.h"

#define RM_MALLOC_FAILED        20
#define RM_INVALID_TABLE        21
#define RM_TABLE_DN_EXIST       22 
#define RM_COLUMN_DN_EXIST      23  
#define RM_RECORD_DESC_DN_EXIST 24  
#define RM_INVALID_RESULT       25  
#define RM_SYSTEM_NULL_VALUE    26  

using namespace std;

# define RM_EOF (-1)  // end of a scan operator

typedef uint32_t TableNum;
typedef uint32_t ColumnNum;
typedef uint32_t NullType;

const NullType TypeNotNull = 0;
const NullType TypeNull = 1;

typedef enum {
    ORIG_ATTRS,
    ORIG_MINUS_ORIGDELS,
    ALL_MINUS_ORIGDELS,
    ALL_ATTRS,           // THIS MUST BE SECOND TO LAST
    ORIGDELS             // THIS MUST BE LAST
} AttrsType;


// Possible Ways of Returning Attributes:
// 1. orig                // col_position <= orig_size
// 2. orig-deleted        // col_position <= orig_size & !column_null
// 3. orig-deleted+added  // !column_null
// 4. orig+deleted+added  //  

// Who would want? 
// 2. orig                // recordData
// 3. orig-deleted        // returnData
// 4. orig-deleted+added  // user
// 4. orig+deleted+added  // system (cleanup or stats)
 

class RecordInfo {
public:
    vector<string> attrNames;
    vector<Attribute> recordDesc;
    vector<bool> nulls; // bad fake bitmap
    vector<int32_t> ints;
    vector<float> reals;
    vector<string> varChars;
    bool hasSomeNull;

    RecordInfo() {
        hasSomeNull = false;
    }
 
    void syncAttrNames() {
        attrNames.clear();
        for (unsigned i = 0; i < recordDesc.size(); ++i) {
            attrNames.push_back(recordDesc[i].name);
        }
    }

    void clearValues() {
        hasSomeNull = false;
        nulls.clear();
        ints.clear();
        reals.clear();
        varChars.clear();
    }
};

// RM_ScanIterator is an iteratr to go through tuples
class RM_ScanIterator {
public:
  RM_ScanIterator();
  ~RM_ScanIterator();

  // "data" follows the same format as RelationManager::insertTuple()
  RC getNextTuple(RID &rid, void *data);
  RC close();

    // Let RelationManager access our private members
    friend class RelationManager;
private:
    RBFM_ScanIterator rbfm_scanIterator;
};


// Relation Manager
class RelationManager
{
public:
  static RelationManager* instance();

  RC createCatalog();

  RC deleteCatalog();

  RC createTable(const string &tableName, const vector<Attribute> &attrs);

  RC deleteTable(const string &tableName);

  RC getAttributes(const string &tableName, vector<Attribute> &attrs);

  RC insertTuple(const string &tableName, const void *data, RID &rid);

  RC deleteTuple(const string &tableName, const RID &rid);

  RC updateTuple(const string &tableName, const void *data, const RID &rid);

  RC readTuple(const string &tableName, const RID &rid, void *data);

  // Print a tuple that is passed to this utility method.
  // The format is the same as printRecord().
  RC printTuple(const vector<Attribute> &attrs, const void *data);

  RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

  // Scan returns an iterator to allow the caller to go through the results one by one.
  // Do not store entire results in the scan iterator.
  RC scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparison type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RM_ScanIterator &rm_ScanIterator);

  // catalog helpers
  RC isStoredTable(const string &tableName);
  RC getTableInfo(const string &tableName, TableNum &tableId); 
  RC getTableInfo(const string &tableName, RID &rid, TableNum &tableId); 
  RC getTableInfo(const string &tableName, RID &rid, TableNum &tableId, ColumnNum &orig_size, ColumnNum &last_column); 
  RC getTableColumnsInfo(TableNum table_id, ColumnNum orig_size, ColumnNum last_column, vector<Attribute> &tableColumns, vector<unsigned> &origDeleteIndexes, vector<RID> rids);

protected:
  RelationManager();
  ~RelationManager();

private:
  static RelationManager *_rm;
  static RecordBasedFileManager *_rbfm;
 
  vector<string>     _rm_catalogNames;   // System_table names
  vector<RecordInfo> _rm_catalogInfos;   // System_table record_info 
  vector<RecordInfo> _rm_catalogQueries; // System_table query_info
  vector<FileHandle> _rm_catalogHandles; // System fileHandles

  TableNum _table_id;

  // Private methods ===========================

  // USE THESE ------------
  
  // file helpers
  bool fileExists(const string &fileName);
  RC openTable(const string &tableName, FileHandle &fh);
  RC closeTable(FileHandle &fh);
  
  // misc helpers
  void addToRecordDescriptor(vector<Attribute> &recordDescriptor, const string &name, AttrType type, AttrLength length); 
  int prepareRecord(const vector<Attribute> &recordDescriptor, vector<void*> valuePointers, void *data); 

  RC getDataFromRecordInfo(const RecordInfo &recordInfo, void* data);
  RC getRecordInfoFromData(const void* data, RecordInfo &recordInfo); 
  RC getSubRecordDescriptor(const vector<Attribute> &origRecordDesc, vector<Attribute> &newRecordDesc, const vector<string> attrNames);

  // table checks
  bool isSystemTable(const string &tableName);

  // catalog helpers
  RC insertTableIntoCatalog(const string &tableName, const vector<Attribute> &attrs);
void getTableFromTables(const string &tableName, TableNum &tableId, int &orig_size, int &last_column, RID &rid);

  // print functions for debugging 
  void printRecordDesc(const vector<Attribute> &rd);
  void printRecordInfo(const RecordInfo &ri);
  void printStrings(const vector<string> &strings);
  RC printRids(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const vector<RID> &rids, unsigned count, unsigned start);

  // SUBHELPERS, DON'T USE THESE ------------
  
  // initializers
  void initCatalogDescriptor_Tables();
  void initCatalogDescriptor_Columns();
  void initCatalogResults_Tables();

  // catalog subhelpers
  RC insertTableIntoTables(const string &tableName, const vector<Attribute> &columns, TableNum &tableId);
  RC insertColumnIntoColumns(TableNum table_id, Attribute column, ColumnNum column_index, NullType column_null);
  RC insertColumnsIntoColumns(TableNum table_id, const vector<Attribute> &columns);
};

#endif
