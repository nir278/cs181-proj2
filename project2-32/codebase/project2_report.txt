﻿1. Basic information
Team Number : 32
Student ID# of Submitter: 1359472
Name of Submitter: Jackson Lento-Edrich
ID#s and Names for others on the Team
1563838 Jean Park
1246278 Nir Jacobson


2. Internal Record Format
We used a variable length record format which stores the number of fields, null bits, pointers to the end of each field, and then the records. This design satisfies 0(1) field access because any field can be located directly. The record is located by rid, which gives pageid and slot number, and the slot contains a pointer to the record. Then, the nth field can be located using the field pointers. The initial offset is the size of the field count, the null bits, and the pointers.
- Describe how you store a VarChar field.
A varchar field stores an int that holds the number of characters n, and then the characters are stored in n bytes. No null terminator is required because there can be at most n characters.



3. Page Format
The slot directory begins at the end of the page right before the free space ptr. From the end, a pointer to the start of free space is stored, then an int holding the total number of slots, and then slot 0, then slot Nth. Each slot contains a record size and a pointer to the start of the record. The position of each slot is calculated backwards from the end of the page, because each page is a fixed size of 4096 bytes. 



4. Table format
For the table format we set it up so each table has it’s own pages and record handler, to navigate to the the tables we have a main file “tables”, here forth called page1, where we store the location of each preceding table, we then have a page called “columns”, here forth called page2, which store the details of each table IE how much data is stored in each tuple for a table and what kind of data. All the data for page1 and page 2 is store with the record format. So if we wanted to create a new table called “example” then we insert a record into page1 with a record containing the table name and the filename where we will store the data for this new table, we then store into page2 the data for the format of the tuples that will be put into table example. 



5. scan Detail



6. Implementation Detail




All data access will take O(1) time, since it is a two step process. First we get to our record by decrementing a pointer to the end of the page by the slot number we want times the size of a slot and the size of the free space pointer. Then when we want to get the field data we take info from the slot to have pointer to the front of the data and to know it size. We now have the block of data we want. This process does not get any longer with more slots since the number of calculations (1 multiplication, and 1 addition) stay static. So it is O(1) time.


For reading data from a specific record, we start with our record manager, we tell our record manager the page and slot we like to see, it in turn evokes methods from our page manager to fetch page, the record manager then parses through the page for the data we requested.




For writing, we again use our record manager to use our page manager to call up pages, except this time we call up pages in order, starting with page one, looking for a page with enough free space for our data we want to write, if one can not be found we will create a new page and write to that. Once we have written our data to the desired page we give it back to the page manager to commit to memory.



7. Other (optional)
- Freely use this section to tell us about things that are related to the project 1, but not related to the other sections (optional)
